# WebRádio

## Arquitetura de uma WebRádio

![diagrama](diagrama.svg)

- **Gravação:** do áudio para transmissão ao vivo e/ou para um arquivo que poderá ser repetido depois. Programas: [Butt](http://danielnoethen.de) ou [Mixx](http://mixxx.org).
- **Administração:** da playlist, arquivos de áudio, usuários administradores e da transmissão ao vivo. Programas: LibreTime ou AutoDJ.
- **Distribuição:** do áudio para múltiplos ouvintes. Programas: IceCast ou ShoutCast.
- **Recepção:** do áudio pelas pessoas. Programas: navegador (Firefox, Chromium etc) ou aplicativo.

([Fonte dos ícones do diagrama](https://commons.wikimedia.org/wiki/Tango_icons))

## Endereços na Internet

**IP:** (Internet Protocol), endereço numérico de qualquer dispositivo (computadores, servidores, celulares etc) em uma rede, incluindo a Internet. Composto por 4 números de 0 à 255, ou seja, vai de 0.0.0.0 à 255.255.255.255.
Por exemplo, o IP da UOL é, nesse momento, [200.147.36.65](http://200.147.36.65).
([mais informações](https://pt.wikipedia.org/wiki/Endere%C3%A7o_IP))

**DNS:** (Domain Name System) sistema que associa nomes aos endereços IP, buscando que sejam mais memorizáveis.
Por exemplo, o nome uol.com.br está, nesse momento, associado ao IP 200.147.36.65.
([mais informações](https://pt.wikipedia.org/wiki/Sistema_de_Nomes_de_Dom%C3%ADnio))


